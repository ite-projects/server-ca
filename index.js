require('dotenv').config();
const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http, {
    cors: {origin: "*", methods: "*"}
});
const cors = require('cors');
app.use(cors());
const InitCA = require("./init/generatorCaCertAndKeys");

InitCA.initServerCa();

const onConnection = require('./socket/pageManger');

io.on('connection', onConnection);

http.listen(process.env.PORT, () => {
    console.log('listening on *:'+process.env.PORT);
});





// // const fs = require("fs");
// const VerifyCertificate = require("./helper/VerifyCertificate");
// const csrGenerator = require("./csr-generator");
// const GeneratorCertificate = require("./helper/GeneratorCertificate");
// const rsaHandler = require('./rsa-handler.js');
//
// const {privateKey, publicKey,} = rsaHandler.generatePublicPrivatePairOfKeys();
//
// const csr = csrGenerator.generateCSR(privateKey, publicKey);
//
// let certPem = GeneratorCertificate.generateCertificateFromCSR(csr)
//
// VerifyCertificate.verifyCertificate(certPem);






