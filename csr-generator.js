require('dotenv').config();

function generateCSR(privateKeyClient, publicKeyClient){


    const forge = require("node-forge");
    const pki = forge.pki;


    const prKeyClient  = pki.privateKeyFromPem(privateKeyClient);
    const pubKeyClient = pki.publicKeyFromPem(publicKeyClient);

    // create a certification request (CSR)
    const csr = pki.createCertificationRequest();

    csr.publicKey = pubKeyClient;
    csr.setSubject([
        {
            name: "commonName",
            value: "example.org",
        },
        {
            name: "countryName",
            value: "US",
        },
        {
            shortName: "ST",
            value: "Virginia",
        },
        {
            name: "localityName",
            value: "Blacksburg",
        },
        {
            name: "organizationName",
            value: "Test",
        },
        {
            shortName: "OU",
            value: "Test",
        },
    ]);

    // set (optional) attributes
    csr.setAttributes([
        {
            name: "challengePassword",
            value: "password",
        },
        {
            name: "unstructuredName",
            value: "My Company, Inc.",
        },
        {
            name: "extensionRequest",
            extensions: [
                {
                    name: "subjectAltName",
                    altNames: [
                        {
                            // 2 is DNS type
                            type: 2,
                            value: 'loca',
                        },
                        {
                            type: 2,
                            value: '15+41.1565',
                        },
                        {
                            type: 2,
                            value: "www.domain.net",
                        },
                    ],
                },
            ],
        },
    ]);

    // sign certification request
    csr.sign(prKeyClient);

    // verify certification request
    const verified = csr.verify();

    console.log('verified CSR = ', verified);

    // convert certification request to PEM-format
    const csrPem = pki.certificationRequestToPem(csr);

    // convert a Forge certification request from PEM-format
    // const csr = forge.pki.certificationRequestFromPem(pem);

    // get an attribute
    // csr.getAttribute({ name: "challengePassword" });

    // get extensions array
    // csr.getAttribute({ name: "extensionRequest" }).extensions;
    return csrPem;
}

module.exports = {
    generateCSR,
}
