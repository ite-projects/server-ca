const GeneratorCertificate = require('../helper/GeneratorCertificate')
const Verify = require('../helper/VerifyCertificate')
const createResponse = require('../socket/response');


const onConnection = (socket) => {
    // socket.id

    socket.on('requestCreateCertForServer', async function (res) {

        let {body} = res;

        let certPem = GeneratorCertificate.generateCertificateFromCSR(body.csrPem)
        let data = createResponse.sendResponse({certPem: certPem})

        console.log('on requestCertForServer data = ', data);
        socket.emit('sendCertToServer', data)
    });

    socket.on('requestVerifyCert', async function (res) {

        let {body} = res;
        let data;
        let status = Verify.verifyCertificate(body.certPem)
        if (status) {
            data = createResponse.sendResponse("Certificate got verified successfully.")
        }else {
            data = createResponse.sendError('Failed to verify certificate')
        }

        console.log('on requestVerifyCert data = ', data);
        socket.emit('sendResultVerifyCert', data)
    });
};

module.exports = onConnection;



