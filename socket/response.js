
function sendResponse(data) {
    return {
        status: true,
        body: data
    }
}

function sendError(msg) {
    return {
        status: false,
        msg: msg
    }
}


module.exports = {
    sendError,
    sendResponse,
};
