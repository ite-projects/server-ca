const fs = require("fs");
const RSA = require("../helper/RSA_Handler");

const attrs = [
    {
        name: "commonName",
        value: "rootCA.org",
    },
    {
        name: "countryName",
        value: "SY",
    },
    {
        shortName: "ST",
        value: "SYRIA",
    },
    {
        name: "localityName",
        value: "DAMAS",
    },
    {
        name: "organizationName",
        value: "DAMASCUS UNIVERSITY",
    },
    {
        shortName: "OU",
        value: "Testy",
    },
]

function generateKeys() {

    const Keys = RSA.generatePublicPrivatePairOfKeys();
    const RootCAPrivateKey = Keys.privateKey;
    const RootCAPublicKey = Keys.publicKey;

    // Writing to files
    fs.writeFileSync("keys/root-ca/private-key.pem", RootCAPrivateKey, {
        encoding: "utf-8", flag: 'w+'
    });
    fs.writeFileSync("keys/root-ca/public-key.pem", RootCAPublicKey, {
        encoding: "utf-8", flag: 'w+'
    });
}

function generateCertificateCA() {

    const forge = require("node-forge");
    const pki = forge.pki;

    // load keys
    const privateKeyCaPem = fs.readFileSync("keys/root-ca/private-key.pem", {
        encoding: "utf-8",
    });

    const publicKeyCaPem = fs.readFileSync("keys/root-ca/public-key.pem", {
        encoding: "utf-8",
    });

    const prKeyCa = pki.privateKeyFromPem(privateKeyCaPem);
    const pubKeyCa = pki.publicKeyFromPem(publicKeyCaPem);


    // create a new certificate
    const cert = pki.createCertificate();

    // fill the required fields
    cert.publicKey = pubKeyCa;
    cert.serialNumber = "01";
    cert.validity.notBefore = new Date();
    cert.validity.notAfter = new Date();
    cert.validity.notAfter.setFullYear(
        cert.validity.notBefore.getFullYear() + 1
    );

    // here we set subject and issuer as the same one
    cert.setSubject(attrs);
    cert.setIssuer(attrs);

    // the actual certificate signing
    cert.sign(prKeyCa);

    let certPem = pki.certificateToPem(cert);

    // Writing to file
    fs.writeFileSync("ssl/ca/is-ca.pem", certPem, {
        encoding: "utf-8", flag: 'w+'
    });

    // now convert the Forge certificate to PEM format
    return true;
}

function initServerCa() {

    const pathPrKey = "./keys/root-ca/private-key.pem";
    const pathPubKey = "./keys/root-ca/public-key.pem";
    const pathCa = "./ssl/ca/is-ca.pem";

    try {
        if (fs.existsSync(pathPrKey) && fs.existsSync(pathPubKey) && fs.existsSync(pathCa)) {
            console.log("all File need exist.")
        } else {
            generateKeys();
            let status = generateCertificateCA();
            if (status){
                console.log("create certificate Done");
            }
        }
    } catch(err) {
        console.error(err)
    }
}

module.exports = {
    initServerCa,
}