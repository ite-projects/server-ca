const forge = require("node-forge");
const CSR = require("./CSR");
const fs = require("fs");

function generateCertificateFromCSR(csrPem) {

    if (! CSR.verifyCSR(csrPem)) {
        return false;
    }

    const csr = forge.pki.certificationRequestFromPem(csrPem);

    // Read CA cert and key
    const caCertPem = fs.readFileSync("ssl/ca/is-ca.pem", {
        encoding: "utf-8",
    });
    const caPrKeyPem = fs.readFileSync("keys/root-ca/private-key.pem", {
        encoding: "utf-8",
    });

    const caCert = forge.pki.certificateFromPem(caCertPem);
    const caPrKey = forge.pki.privateKeyFromPem(caPrKeyPem);

    console.log("Creating certificate...");
    const cert = forge.pki.createCertificate();
    cert.publicKey = csr.publicKey;
    cert.serialNumber = "02";//todo ask
    cert.validity.notBefore = new Date();
    cert.validity.notAfter = new Date();
    cert.validity.notAfter.setFullYear(
        cert.validity.notBefore.getFullYear() + 1
    );

    // subject from CSR
    cert.setSubject(csr.subject.attributes);
    // issuer from CA
    cert.setIssuer(caCert.subject.attributes);

    cert.setExtensions([
        {
            name: "basicConstraints",
            cA: true,
        },
        {
            name: "keyUsage",
            keyCertSign: true,
            digitalSignature: true,
            nonRepudiation: true,
            keyEncipherment: true,
            dataEncipherment: true,
        },
        {
            name: "subjectAltName",
            altNames: [
                {
                    type: 6, // URI
                    value: "http://example.org/webid#me",
                },
            ],
        },
    ]);

    cert.sign(caPrKey);

    console.log("Certificate created.");

    let certPem = forge.pki.certificateToPem(cert)
    console.log("Writing Certificate");

    fs.writeFileSync("ssl/is-cert.pem", certPem,{encoding: "utf-8", flag: 'w+'});

    return certPem;
}

module.exports = {
    generateCertificateFromCSR,
}