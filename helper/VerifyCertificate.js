const pki = require("node-forge").pki;
const fs = require("fs");

function verifyCertificate(certPem) {

    // get Cert CA
    let caStore;
    try {
        let caCert = fs.readFileSync("ssl/ca/is-ca.pem", {encoding: "utf-8",});
        caStore = pki.createCaStore([caCert]);
    } catch (e) {
        console.log("Failed to load CA certificate (" + e + ")");
        return false;
    }

    // verify Certificate
    try {
        const certToVerify = pki.certificateFromPem(certPem);
        const verified = pki.verifyCertificateChain(caStore, [certToVerify]);
        if (verified) {
            console.log("Certificate got verified successfully.!");
        }
        return verified;
    } catch (e) {
        console.log("Failed to verify certificate (" + (e.message || e) + ")");
        return false;
    }
}

module.exports = {
    verifyCertificate,
}