const forge = require("node-forge");
const fs = require("fs");

function verifyCSR(csrPem) {

    const csr = forge.pki.certificationRequestFromPem(csrPem);

    if (csr.verify()) {
        console.log("Certification request (CSR) verified.");
        return true;
    } else {
        console.log("Certification request (CSR) Signature not verified.");
        return false;
    }
}

module.exports = {
    verifyCSR,
}